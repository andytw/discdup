const {dialog} = require('electron').remote;
var fs = require('fs');

(function() {
    var makeJDF = function(formid) {
        var form = document.getElementById(formid);
        var id = form.elements["id"];
        var copies = form.elements["copies"];
        var label = form.elements["label"];
        
        if ("audiocd" == formid ) {
            var tracks = document.getElementsByClassName('tracks');
        }

        if ( id == "" ) {
            dialog.showErrorBox('Missing Info:', 'Please fill in the Job Name.');
            return;
        }
  
        var text = "";
  
        text = 
            'JOB_ID=' + id.value.replace(/[^\w-_]/g,'_') + '\r\n' +
            'PUBLISHER=PP-100II' + '\r\n' +
            'COPIES=' + copies.value + '\r\n';

        if ( "dvd" == formid ) {
            text += 'DISC_TYPE=DVD' + '\r\n';
        } else {
            text += 'DISC_TYPE=CD' + '\r\n';
        }

        if ( "audiocd" == formid ) {
            for (var i = 0, len = tracks.length; i < len; i++ ) {
                if ( tracks[i].files[0] && tracks[i].files[0].path !== null ) {
                    text += 'AUDIO_TRACK=PATH:' + tracks[i].files[0].path + '\r\n';
                } 
            }
        }

        if ( "mp3cd" == formid ) {
            text += 'FORMAT=JOLIET' + '\r\n';
            
            for (var j = 0; j < form.mp3files.files.length; j++) {
                text += 'DATA=' + form.mp3files.files[j].path + '\r\n';
            }
        }

        if ( "dvd" == formid && form.image.files[0] ) {
            text += 'IMAGE=' + form.image.files[0].path + '\r\n';
            text += 'CLOSE_DISC=YES' + '\r\n';
        }

        text += 'LABEL=' + label.files[0].path + '\r\n';
        text += 'LABEL_AREA=DiscDiamOut:1190' + '\t' + 'DiscDiamIn:210'
  
        let content = text;

        dialog.showSaveDialog(
            {
            filters: [
                {name: 'JDF File', extensions: ['JDF']},
            ]
            },
            (fileName) => {
                if (fileName === undefined){
                    console.log("You didn't save the file.");
                    return;
                }
        
                // fileName is a string that contains the path and filename created in the save file dialog.  
                fs.writeFile(fileName, content, 'latin1', (err) => {
                    if(err){
                        dialog.showErrorBox("An error occurred creating the file.", err.message);
                    }       
                });
            }
        ); 
    };
  
    var createBtns = document.getElementsByClassName('create');
    
    for ( let k = 0; k < createBtns.length; k++ ) {
        createBtns[k].addEventListener('click', function(el) {
            makeJDF(el.target.dataset.form);
        })
    }

    form = document.getElementById('audiocd');
    form.addEventListener('click', function(e) {
        if (e.target && e.target.className == 'addtrack' ) {
            newRow = 
                '<label for="audio' + e.target.dataset.number + '">Audio Track:</label>' +
                '<input id="audio' + e.target.dataset.number + '" class="tracks" type="file">' +
                '<button class="addtrack" data-number="' + ( +e.target.dataset.number + 1 ) + '" type="button">Add Track</button>';
            e.target.insertAdjacentHTML('afterend', newRow );
        }
    });

})();
  